module.exports = {
    purge: ["./public/**/*.html"],
    theme: {
        extend: {
            maxHeight: theme => ({
                "screen/2": "50vh",
                "screen/3": "calc(100vh / 3)",
                "screen/4": "25vh",
                "screen/5": "20vh",
            }),
            colors: {
                'white': '#fff',
                'neutral': '#4c4c4c',
                'primary': {
                    'lighten': '#2039cc',
                    'default': '#111e6c',
                    'darken': '#0c154a',
                },
            }
        },
        fontFamily: {
            'sans': ['Poppins', 'sans-serif'],
            'serif': ['Poppins', 'sans-serif'],
        },
    },
    variants: {},
    plugins: [],
};
